# We are vastly simpler here than the normal Dracut iscsi root support; if
# we are booted via iscsi, we use iscsistart to set up networking
# according to the passed in information and make the drives
# available.

modprobe -q iscsi_ibft || exit 1

#while ! [ -f /sys/module/iscsi_ibft/initstate -a \( "`cat /sys/module/iscsi_ibft/initstate`" == live \) ] ; do
#    sleep 0.1
#done

[ -d /sys/firmware/ibft/ethernet0 ] || exit 0

# Force the module to be loaded for the ethernet device; relying on udev
# to do this doesn't seem to be reliable (possibly because there's a
# race condition in it being loaded before this point; possibly because
# we're simply missing the necessary udev rules.)
modprobe `cat /sys/firmware/ibft/ethernet0/device/modalias`

systemd-cat /sbin/iscsistart -N

cat > $hookdir/initqueue/gnome-hwtest-iscsistart.sh <<EOF
if [ "\`cat /sys/firmware/ibft/ethernet0/device/net/*/carrier\`" == 1 ] ; then
    systemd-cat /sbin/iscsistart -b
    > /tmp/gnome-hwtest.did-iscsistart
    rm $hookdir/initqueue/gnome-hwtest-iscsistart.sh
fi
EOF

echo '[ -f /tmp/gnome-hwtest.did-iscsistart ]' >  $hookdir/initqueue/finished/gnome-hwtest.sh
