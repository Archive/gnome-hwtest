const Lang = imports.lang;
const Format = imports.format;
const GLib = imports.gi.GLib;

function ConfigFileError(configfile, message) {
    this.message = configfile.file.get_basename() + ': ' + message;
    this.stack = (new Error()).stack;
}

ConfigFileError.prototype = Object.create(Error.prototype, {});

const ConfigFile = new Lang.Class({
    Name: 'ConfigFile',

    _init: function(file) {
        this.file = file;
        this.keyfile = new GLib.KeyFile();
        try {
            this.keyfile.load_from_file(file.get_path(), 0);
        } catch (e) {
            throw new ConfigFileError(this, Format.vprintf("can't open file: %s", [e.message]));
        }
    },

    _getValue: function(group, key, default_, transform) {
        try {
            let val = this.keyfile.get_value(group, key);
            if (transform !== undefined) {
                try {
                    return transform(val);
                } catch (e) {
                    throw new ConfigFileError(this, Format.vprintf("%s/%s: %s",
                                                                   [group, key, e.message]));
                }
            } else {
                return val;
            }
        } catch (e if e.matches(GLib.KeyFileError, GLib.KeyFileError.KEY_NOT_FOUND)) {
            if (default_ === undefined)
                throw new ConfigFileError(this, Format.vprintf("%s/%s: missing parameter",
                                                               [group, key]));
            else
                return default_;
        }
    },

    getString: function(group, key, default_) {
        return this._getValue(group, key, default_);
    },

    getInt: function(group, key, default_) {
        return this._getValue(group, key, default_,
                               function(val) {
                                   let numVal = Number(val);
                                   if (numVal % 1 === 0)
                                       return numVal;
                                   else
                                       throw new Error("not an integer");
                                   });
    },

    getBool: function(group, key, default_) {
        return this._getValue(group, key, default_,
                               function(val) {
                                   if (val == 'true')
                                       return true;
                                   else if (val == 'false')
                                       return false;
                                   else
                                       throw new Error("should be true or false");
                                   });
    },

    getGroups: function() {
        return this.keyfile.get_groups();
    }
});
