let Gio = imports.gi.Gio;

let Partition = imports.partition;
let Utils = imports.utils;

let logger = new Utils.Logger('gnome-hwtest-iscsi');

function tgtadm(lld, mode, op, ...rest) {
    let args = ['tgtadm'];
    if (lld != null)
        args.push('--lld', lld);
    args.push('--mode', mode, '--op', op);

    args.push.apply(args, rest);
    Utils.runSync(args);
}

function prepareBootImages(config) {
    let imageDir = Gio.File.new_for_path('/srv/gnome-hwtest/images');
    Utils.mkdir_p(imageDir);

    let names = Set();
    for (let machine of Utils.values(config.machines))
        for (let partition of Utils.values(machine.partitions))
            names.add(partition.name);

    names.add('minimal');

    for (let name of names) {
        if (config.export_root_via_iscsi && name == 'minimal')
            continue;

        logger.info('Creating %s', Partition.bootImage(name));
        let subprocess = Utils.subprocessCreate(['gnome-hwtest-make-boot-image',
                                                 Partition.bootImage(name),
                                                 Partition.bootLabel(name)]);
        subprocess.wait(null);
        Utils.subprocessCheck(subprocess);
    }

    return names;
}

let commands = {
    'configure' : function(config) {
        let names = prepareBootImages(config);

        tgtadm(null, 'sys', 'update', '--name', 'State', '-v', 'offline');

        let tid = '1';
        for (let name of names) {
            tgtadm('iscsi', 'target', 'new', '--tid', tid, '--targetname', Partition.bootIqn(name));
            tgtadm('iscsi', 'target', 'bind', '--tid', tid, '--initiator-address', 'ALL');

            if (config.export_root_via_iscsi && name == 'minimal') {
                tgtadm('iscsi', 'logicalunit', 'new', '--tid', tid, '--lun', '1', '--backing-store', '/dev/disk/by-partlabel/gnome-hwtest-controller-export');
            } else {
                tgtadm('iscsi', 'logicalunit', 'new', '--tid', tid, '--lun', '1', '--backing-store', Partition.bootImage(name));
            }

            tgtadm('iscsi', 'logicalunit', 'update', '--tid', tid, '--lun', '1', '-P', 'readonly=1');

            tid = String(Number(tid) + 1);
        }

        tgtadm(null, 'sys', 'update', '--name', 'State', '-v', 'ready');

        return true;
    },

    'stop' : function() {
        // If there are leaked sessions (because a target crashed, say) we still want to
        // tear things down immediately; everything we export is read-only so there
        // is no danger of data loss.

        let args = ['tgtadm', '--lld', 'iscsi', '--mode', 'target', '--op', 'show'];

        let target, session, connection;
        let targets = [];

        for (let line of Utils.runIterateLines(args)) {
            let words = line.trim().split(/\s+/);
            if (words[0] == 'Target') {
                target = words[1].replace(':', '');
                targets.push(target);
            }
        }

        for (target of targets) {
            args = ['tgtadm', '--lld', 'iscsi', '--mode', 'conn', '--op', 'show', '--tid', target];

            for (let line of Utils.runIterateLines(args)) {
                let words = line.trim().split(/\s+/);

                if (words[0] == 'Session:') {
                    session = words[1];
                } else if (words[0] == 'Connection:') {
                    connection = words[1];
                    tgtadm('iscsi', 'conn', 'delete', '--tid', target, '--sid', session, '--cid', connection);
                }
            }

            tgtadm('iscsi', 'target', 'delete', '--tid', target);
        }

        tgtadm('iscsi', 'sys', 'delete');

        return true;
    }
};
