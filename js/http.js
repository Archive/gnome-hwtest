let Format = imports.format;
let Gio = imports.gi.Gio;
let Utils = imports.utils;

let commands = {
    'update-config': function(config) {
        let rundir = Gio.file_new_for_path('/run/gnome-hwtest/');

        Utils.mkdir_p(rundir.get_child('http_root'));
        Utils.mkdir_p(rundir.get_child('params'));

        Utils.outputToFile(rundir.get_child('lighttpd.conf'),
        function(w) {
            w('server.modules = ( "mod_alias" )');
            w('');
            w('server.port = 80');
            w('server.bind = "%s"', config.private_ip);
            w('');
            w('server.document-root = "/run/gnome-hwtest/http_root"');
            w('');
            w('alias.url = ( "/repo/" => "/srv/gnome-hwtest/repo/" )');

            for (let machine of Utils.values(config.machines)) {
                w('');
                w('$HTTP["remoteip"] == "%s" {', machine.ip);
                w('    alias.url += ( "/params" => "/run/gnome-hwtest/params/%s.params" )', machine.name);
                w('}');
            }
        });

        if (Utils.systemdUnitStatus('gnome-hwtest-controller-http.service') == 'active')
            Utils.runSync(['systemctl', 'restart', 'gnome-hwtest-controller-http.service']);

        return true;
    }
}
