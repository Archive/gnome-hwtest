let Format = imports.format;
let Gio = imports.gi.Gio;
let Lang = imports.lang;

let Utils = imports.utils;

function PowerError(message) {
    this.message = message;
    this.stack = (new Error()).stack;
}

PowerError.prototype.toString = Error.prototype.toString;

const PowerController = new Lang.Class({
    Name: 'PowerController',

    _init: function(power_tty) {
        /* approximation of locking */
        let fuser = Gio.Subprocess.new(['fuser', power_tty],
                                       Gio.SubprocessFlags.STDOUT_SILENCE | Gio.SubprocessFlags.STDERR_SILENCE);
        fuser.wait(null);
        if (fuser.get_exit_status() == 0)
            throw new PowerError("'" + power_tty + "' is busy");

        let stty = Gio.Subprocess.new(['stty', '-F', power_tty, 'raw', '-echo', '-hup'],
                                      Gio.SubprocessFlags.NONE);
        stty.wait (null);

        let ttyFile = Gio.File.new_for_path(power_tty);

        this.powerin = Gio.DataInputStream.new(ttyFile.read(null));
        this.powerout = Gio.DataOutputStream.new(ttyFile.append_to(Gio.FileCreateFlags.NONE, null));

        let token = this._makeToken();
        this.powerout.put_string('ping ' + token + '\n', null);
        let sentSecondToken = false;
        while (true) {
            let [line,] = this.powerin.read_line_utf8(null);
            line = line.trim();
            if (line.startsWith('ok ')) {
                let rest = line.substr(3).trim();
                if (rest == token)
                    break;
            }
            if (!sentSecondToken) {
                this.token = this._makeToken();
                this.powerout.put_string('ping ' + token + '\n', null);
                sentSecondToken = true;
            }
        }
    },

    _makeToken: function() {
        let instream = Gio.File.new_for_path('/dev/urandom').read(null);
        let bytes = instream.read_bytes(8, null).toArray();
        instream.close(null);

        let token = '';
        for (let i = 0; i < bytes.length; i++) {
            let val = bytes[i];
            token += (val % 16).toString(16) + (val >> 4).toString(16);
        }

        return token;
    },

    _waitForResponse: function() {
        let [line,] = this.powerin.read_line_utf8(null);
        line = line.trim();
        if (line == 'ok')
            return '';
        else if (line.startsWith('ok '))
            return line.substr(3);
        else
            throw new PowerError(line);
        /*
        else if (line.startsWith('err '))
            throw new PowerError(line.substr(4));
        else
            throw new PowerError("Bad response");
            */
    },

    on: function(which) {
        this.powerout.put_string('on ' + which + '\n', null);
        this._waitForResponse();
    },

    off: function(which) {
        this.powerout.put_string('off ' + which + '\n', null);
        this._waitForResponse();
    },

    query: function() {
        this.powerout.put_string('query\n', null);
        let response = this._waitForResponse();
        let result = [];
        for (let i = 0; i < response.length; i++) {
            if (response[i] == '0')
                result.push(false);
            else if (response[i] == '1')
                result.push(true);
            else
                throw new PowerError("Bad character in response");
        }
        return result;
    }
});

function getControllerByTTY(config, tty) {
    if (config._PowerControllers == null)
        config._PowerControllers = {};

    if (!(tty in config._PowerControllers)) {
        config._PowerControllers[tty] = new PowerController(tty);
    }

    return config._PowerControllers[tty];
}

function getController(config, machine) {
    return getControllerByTTY(config, machine.power_tty);
}

let commands = {
    'on' : function(config, args) {
        if (args.length != 1) {
            Utils.printerr("Usage: gnome-hwtest power on <machine_name>\n");
            return false;
        }

        let machineName = args[0];
        if (!(machineName in config.machines)) {
            Utils.printerr("machine '%s' is not known", machineName);
            return false;
        }

        let machine = config.machines[machineName];
        let controller = getController(config, machine);
        try {
            controller.on(machine.power_index);
        } catch (e if e instanceof PowerError) {
            Utils.printerr("Error: %s\n", e.message);
            return false;
        }

        return true;
    },

    'off' : function(config, args) {
        if (args.length != 1) {
            Utils.printerr("Usage: gnome-hwtest power off <machine_name>\n");
            return false;
        }

        let machineName = args[0];
        if (!(machineName in config.machines)) {
            Utils.printerr("machine '%s' is not known", machineName);
            return false;
        }

        let machine = config.machines[machineName];
        let controller = getController(config, machine);
        try {
            controller.off(machine.power_index);
        } catch (e if e instanceof PowerError) {
            Utils.printerr("Error: %s\n", e.message);
            return false;
        }

        return true;
    },

    'query': function(config, args) {
        if (args.length != 0) {
            Utils.printerr("Usage: gnome-hwtest power query\n");
            return false;
        }

        let ttys = [];
        for (let machine of Utils.values(config.machines)) {
            if (ttys.indexOf(machine.power_tty) < 0)
                ttys.push(machine.power_tty);
        }
        ttys.sort();

        for (let tty of ttys) {
            printerr(tty);
            let controller = getControllerByTTY(config, tty);
            let results;
            try {
                results = controller.query();
            } catch (e if e instanceof PowerError) {
                Utils.printerr("Error: %s\n", e.message);
                return false;
            }

            print(tty + ':');
            for (let i = 0; i < results.length; i++) {
                let machineName = null;
                for (let machine of Utils.values(config.machines)) {
                    if (machine.power_tty == tty && machine.power_index == (i + 1)) {
                        machineName = machine.name;
                        break;
                    }
                }
                if (machineName)
                    print(Format.vprintf("   Machine %d (%s): %s",
                                         [i + 1, machineName, results[i] ? 'on' : 'off']));
                else
                    print(Format.vprintf("   Machine %d: %s",
                                         [i + 1, results[i] ? 'on' : 'off']));
            }
        }

        return true;
    }
}
