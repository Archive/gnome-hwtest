const Gio = imports.gi.Gio;
const Format = imports.format;
const Lang = imports.lang;
const Mainloop = imports.mainloop;

const LogListener = imports.loglistener;
const Machine = imports.machine;
const Upload = imports.upload;
const Utils = imports.utils;

const JobRunner = imports.jobrunner;

let logger = new Utils.Logger('gnome-hwtest-controller');

const repodir = Gio.File.new_for_path('/srv/gnome-hwtest/repo');

function getCurrentRef(tree) {
    let file = repodir.get_child('refs/heads/' + tree);
    try {
        let [, contents,, ] = file.load_contents(null);
        return String(contents).trim();
    } catch (e if e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.NOT_FOUND)) {
        return null;
    }
}

function setCurrentRef(tree, ref) {
    let file = repodir.get_child('refs/heads/' + tree);
    Utils.mkdir_p(file.get_parent());
    Utils.outputToFile(file,
                       function(w) {
                           w('%s', ref);
                       });
}

const TEST_TIMEOUT = 300
const POLL_INTERVAL_SEC = 120;

// We have only one poll job that polls all the trees, because if both the controller
// OS and the target trees have changed we want to update the controller OS first.
const PollJob = Lang.Class({
    Name: 'PollJob',
    Extends: JobRunner.Job,

    persists: true,

    _init: function(controller) {
        this.parent(controller);
        this.lastRunTime = 0;

        this.booted_revision = null;
        for (let line in Utils.runIterateLines(['ostree', 'admin', 'status'])) {
            let m = /^\*\s+\S+\s+([0-9a-fA-F]+)\.\d+/.exec(line);
            if (m)
                this.booted_revision = m[1];
        }

        if (this.booted_revision == null)
            this.logger.warning("Cannot determine booted operating system revision");
    },

    toString: function() {
        return Format.vprintf("%s()", [this.__name__]);
    },

    canRun: function() {
        if (Date.now() < this.lastRunTime + POLL_INTERVAL_SEC * 1000)
            return false;

        // Once we start updating the operating system, we don't poll
        for (let job of this.runner.jobs) {
            if (job instanceof HWTestPullJob || job instanceof HWTestUpdateJob)
                return false;
        }

        return true;
    },

    runAsync: function(cancellable, next) {
        this.lastRunTime = Date.now();

        let trees = Set();
        for (let machine of Utils.values(this.runner.machines))
            for (let partition of Utils.values(machine.partitions))
                trees.add(partition.tree);

        trees.add(this.runner.config.hwtest_tree);

        let refs = {};
        for (let tree of trees) {
            let subprocess = Utils.subprocessCreate(['curl', '-s',
                                                     this.runner.config.upstream_repo + '/refs/heads/' + tree],
                                                    Gio.SubprocessFlags.STDOUT_PIPE);
            subprocess.communicate_utf8_async(null, cancellable, next);
            let [, result] = yield cancellable;
            let [, out, ] = subprocess.communicate_utf8_finish(result);
            Utils.subprocessCheck(subprocess);

            refs[tree] = out.trim();
            this.logger.info("Tree is %s", out);
        }

        if (this.booted_revision != null && refs[this.runner.config.hwtest_tree] != this.booted_revision) {
            this.runner.addJob(new HWTestPullJob(this.runner, refs[this.runner.config.hwtest_tree]));
            return;
        }

        for (let tree of trees) {
            let ref = refs[tree];
            let oldRef = getCurrentRef(tree);

            if (oldRef == ref) {
                logger.info("%s: unchanged at %s", tree, ref);
            } else {
                logger.info("%s: %s => %s", tree, oldRef, ref);
                this.runner.addJob(new PullJob(this.runner, tree, ref));
            }
        }
    }
});


const PullJob = Lang.Class({
    Name: 'PullJob',
    Extends: JobRunner.Job,

    _init: function(controller, tree, ref) {
        this.parent(controller);
        this.tree = tree;
        this.ref = ref;
    },

    toString: function() {
        return Format.vprintf("%s(%s; ref=%s)", [this.__name__, this.tree, this.ref]);
    },

    canRun: function() {
        for (let job of this.runner.jobs) {
            if (job instanceof PullJob && job.running)
                return false;
        }

        return true;
    },

    pullSucceeded: function() {
        for (let machine of Utils.values(this.runner.machines)) {
            for (let partition of Utils.values(machine.partitions)) {
                if (partition.tree != this.tree)
                    continue;

                // See if we have a pending update job for this partition
                let pendingJob = null;
                for (let job of this.runner.jobs) {
                    if ((job instanceof UpdateJob && job.partition == partition && job.ref == null)) {
                        pendingJob = job;
                        break;
                    }
                }

                if (pendingJob == null)
                    this.runner.addJob(new UpdateJob(this.runner, partition));
            }
        }
    },

    runAsync: function(cancellable, next) {
        let subprocess = Utils.subprocessCreate(['ostree',
                                                 '--repo=' + repodir.get_path(),
                                                 'pull',
                                                 'gnome-continuous',
                                                 this.ref]);

        subprocess.wait_async(cancellable, next);
        let [, result] = yield cancellable;
        subprocess.wait_finish(result);

        if (subprocess.get_exit_status() == 0) {
            setCurrentRef(this.tree, this.ref);
            logger.info("%s: pull succeeded", this);
            this.pullSucceeded();

        } else {
            logger.error("%s: pull failed", this);
        }
    }
});

const HWTestPullJob = Lang.Class({
    Name: 'HWTestPullJob',
    Extends: PullJob,

    _init: function(controller, ref) {
        this.parent(controller, controller.config.hwtest_tree, ref);
    },

    pullSucceeded: function() {
        this.runner.addJob(new HWTestUpdateJob(this.runner));
    }
});

const HWTestUpdateJob = Lang.Class({
    Name: 'HWTestUpdateJob',
    Extends: JobRunner.Job,

    _init: function(controller) {
        this.parent(controller);
    },

    toString: function() {
        return Format.vprintf("%s()", [this.__name__]);
    },

    canRun: function() {
        // Wait until no jobs are running or pending
        for (let job of this.runner.jobs) {
            if (job.running)
                return false
            if (job != this && !(job instanceof PollJob))
                return false;
        }

        return true;
    },

    runAsync: function(cancellable, next) {
        let subprocess = Utils.subprocessCreate(['gnome-hwtest-update-controller',
                                                 this.runner.config.hwtest_tree],
                                                Gio.SubprocessFlags.NONE);
        subprocess.wait_async(cancellable, next);
        let [, result] = yield cancellable;
        subprocess.wait_finish(result);
        Utils.subprocessCheck(subprocess);

        let subprocess = Utils.subprocessCreate(['systemctl', 'reboot'],
                                                Gio.SubprocessFlags.NONE);
        let [, result] = yield cancellable;
        subprocess.wait_finish(result);
        Utils.subprocessCheck(subprocess);

        // At this point we should be rebooting, just wait indefinitely
        yield;
    },
});

const UpdateJob = Lang.Class({
    Name: 'UpdateJob',
    Extends: JobRunner.Job,

    timeout: 300,

    _init: function(controller, partition) {
        this.parent(controller);
        this.partition = partition;
    },

    toString: function() {
        if (this.ref != null) {
            return Format.vprintf("%s(%s/%s; ref=%s)",
                                  [this.__name__, this.partition.machine.name, this.partition.name, this.ref]);
        } else {
            return Format.vprintf("%s(%s/%s)",
                                  [this.__name__, this.partition.machine.name, this.partition.name]);
        }
    },

    canRun: function() {
        if (this.partition.machine.state != Machine.STATE_INACTIVE)
            return false;

        for (let job of this.runner.jobs) {
            if (job instanceof TestJob && job.partition == this.partition)
                return false;
        }

        return true;
    },

    runAsync: function(cancellable, next) {
        this.ref = getCurrentRef(this.partition.tree);

        let machine = this.partition.machine;

        machine.state = Machine.STATE_UPDATING;

        setCurrentRef(machine.name, this.ref);

        machine.setBootPartitionName('minimal');
        machine.writeParams(this.partition);

        machine.powerOff();
        Mainloop.timeout_add(5000, function() {
            next();
            return false;
        });
        yield;
        machine.powerOn();

        let listenerId = machine.logListener.connect('record', next);

        this.log = [];
        try {
            while (true) {
                let [, record] = yield;
                this.log.push(record);

                if (record['MESSAGE_ID'] == LogListener.SHUTDOWN_MESSAGE_ID)
                    break;
            }
        } finally {
            machine.logListener.disconnect(listenerId);
        }

        // At this point we know that the machine is shutting down and
        // we've received all the messages that we're going to get, but
        // we don't know that it's actually unmounted its partitions and
        // powered off. And we can't know this because by the time
        // systemd unmounts the root partition, all processes have been
        // killed and there's nothing to notify us. So wait a bit for
        // a clean shutdown.

        Mainloop.timeout_add(5000, function() {
            next();
            return false;
        });
        yield;

        logger.info("%s: update succeeded", this);
        for (let testset of Utils.values(this.partition.testsets))
            this.runner.addJob(new TestJob(this.runner, this.partition, this.ref, testset));
    },

    stop: function(message, exception) {
        if (message) {
            /* Since test reports are per-target (including a testset), we log an error in
             * updating against each test-report separately. This is a bit silly, but
             * since we expect normally the test to fail, not the update, this keeps
             * the infrastructure simple.
             */
            for (let testset of Utils.values(this.partition.testsets)) {
                this.runner.addJob(new ErrorUploadJob(this.runner,
                                                      this.partition,
                                                      this.ref,
                                                      testset,
                                                      "Update failed: " + message,
                                                      this.log));
            }
        }

        this.log = null;

        let machine = this.partition.machine;
        machine.powerOff();
        machine.state = Machine.STATE_INACTIVE;
        this.parent();
    }
});

const TestJob = Lang.Class({
    Name: 'TestJob',
    Extends: JobRunner.Job,

    timeout: 300,

    _init: function(controller, partition, ref, testset) {
        this.parent(controller);
        this.partition = partition;
        this.ref = ref;
        this.testset = testset;
    },

    toString: function() {
        return Format.vprintf("%s(%s/%s; ref=%s, testset=%s)",
                              [this.__name__, this.partition.machine.name,
                               this.partition.name, this.ref, this.testset]);
    },

    canRun: function() {
        return this.partition.machine.state == Machine.STATE_INACTIVE;
    },

    runAsync: function(cancellable, next) {
        let machine = this.partition.machine;

        machine.state = Machine.STATE_TESTING;

        machine.setBootPartitionName(this.partition.name);
        machine.writeParams(this.partition, this.testset);

        machine.powerOff();
        Mainloop.timeout_add(5000, function() {
            next();
            return false;
        });
        yield;
        machine.powerOn();

        let listenerId = machine.logListener.connect('record', next);

        let metrics = [];

        this.log = [];
        try {
            while (true) {
                let [, record] = yield;
                this.log.push(record);

                if (record['MESSAGE_ID'] == LogListener.METRIC_MESSAGE_ID) {
                    let metric = {
                        name: record['METRIC_NAME'],
                        value: Number(record['METRIC_VALUE'])
                    };
                    if ('METRIC_UNITS' in record)
                        metric['units'] = record['METRIC_UNITS'];
                    if ('METRIC_DESCRIPTION' in record)
                        metric['description'] = record['METRIC_DESCRIPTION'];

                    metrics.push(metric);
                    logger.info("%s: got metric %s", this, JSON.stringify(metric));
                }

                if (record['MESSAGE_ID'] == LogListener.SHUTDOWN_MESSAGE_ID)
                    break;
            }
        } finally {
            machine.logListener.disconnect(listenerId);
        }

        // See comment above in UpdateJob.runsync()
        Mainloop.timeout_add(5000, function() {
            next();
            return false;
        });
        yield;

        logger.info("%s: test succeeded", this);

        let uploader = Upload.getUploader(this.runner.config);
        uploader.uploadSuccessAsync(this.partition,
                                    this.partition.tree,
                                    this.testset,
                                    this.ref,
                                    new Date(),
                                    metrics,
                                    cancellable, next);

        let [, result] = yield cancellable;
        uploader.uploadFinish(result);

        logger.info("%s: upload succeeded", this);
    },

    stop: function(message, exception) {
        if (message)
            this.runner.addJob(new ErrorUploadJob(this.runner,
                                                  this.partition,
                                                  this.ref,
                                                  this.testset,
                                                  "Test failed: " + message,
                                                  this.log));
        this.log = null;

        let machine = this.partition.machine;
        machine.powerOff();
        machine.state = Machine.STATE_INACTIVE;
        this.parent();
    }
});


const ErrorUploadJob = Lang.Class({
    Name: 'ErrorUploadJob',
    Extends: JobRunner.Job,

    _init: function(controller, partition, ref, testset, errorMessage, log) {
        this.parent(controller);
        this.partition = partition;
        this.ref = ref;
        this.testset = testset;
        this.errorMessage = errorMessage;
        this.log = log;
    },

    toString: function() {
        return Format.vprintf("%s(%s/%s; ref=%s, testset=%s)",
                              [this.__name__, this.partition.machine.name,
                               this.partition.name, this.ref, this.testset]);
    },

    canRun: function() {
        return true;
    },

    runAsync: function(cancellable, next) {
        let uploader = Upload.getUploader(this.runner.config);
        uploader.uploadErrorAsync(this.partition,
                                  this.partition.tree,
                                  this.testset,
                                  this.ref,
                                  new Date(),
                                  this.errorMessage, this.log,
                                  cancellable, next);

        let [, result] = yield cancellable;
        uploader.uploadFinish(result);

        logger.info("%s: error upload succeeded", this);
    }
});

const Controller = Lang.Class({
    Name: 'Controller',
    Extends: JobRunner.JobRunner,

    _init: function(config) {
        this.parent(logger);

        this.config = config;
        this.machines = config.machines;

        for (let machine of Utils.values(this.machines)) {
            machine.ensureLogListener();
            machine.powerOff();
        }

        this.addJob(new PollJob(this));
    },
});

function main(config) {
    let controller = new Controller(config);
    Mainloop.run();

    return true;
}
