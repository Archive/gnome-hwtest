const Format = imports.format;
const Lang = imports.lang;

const ConfigFile = imports.configfile;
const Machine = imports.machine;
const Partition = imports.partition;
const Utils = imports.utils;

function resolveHwaddr(configFile, hwaddr) {
    let iface = Utils.findIface(hwaddr);
    if (iface != null)
        return Utils.ifaceAddr(iface);
    else
        return null;
}
const Config = new Lang.Class({
    Name: 'Config',

    _init: function(file) {
        let configfile = new ConfigFile.ConfigFile(file);

        this.machines = {};

        let [groups,] = configfile.getGroups();
        for (let group of groups) {
            if (group == 'controller') {
                this.private_hwaddr = configfile.getString(group, 'private_hwaddr');
                this.private_ip = resolveHwaddr(configfile, this.private_hwaddr);
                this.public_hwaddr = configfile.getString(group, 'public_hwaddr');
                this.public_ip = resolveHwaddr(configfile, this.private_hwaddr);
                this.upstream_repo = configfile.getString(group, 'upstream_repo', 'https://build.gnome.org/repo');
                this.perf_server_api = configfile.getString(group, 'perf_server_api', 'https://perf.gnome.org/api');
                this.hwtest_tree = configfile.getString(group, 'hwtest_tree', 'gnome-continuous/hwtest-stable/x86_64-hwtest');
                this.export_root_via_iscsi = configfile.getBool(group, 'export_root_via_iscsi', true);

                continue;
            }

            let m = /machine (.*)/.exec(group);
            if (m) {
                let machine = new Machine.Machine(this, m[1], configfile, group);
                this.machines[machine.name] = machine;
            }

            m = /partition ([^\/]*)\/(.*)/.exec(group);
            if (m) {
                let machineName = m[1];
                let machine = this.machines[machineName];
                if (machine === undefined)
                    throw new ConfigFile.ConfigFileError(this,
                                                         Format.vprintf("%s: unknown machine %s\n",
                                                                        group, machineName));

                machine.addPartition(new Partition.Partition(this, machine, m[2], configfile, group));
                continue;
            }
        }
    }
});
