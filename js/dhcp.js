let Format = imports.format;
let Gio = imports.gi.Gio;
let Utils = imports.utils;

let commands = {
    'update-config': function(config) {
        let configdir = Gio.file_new_for_path('/run/gnome-hwtest/');

        Utils.outputToFile(configdir.get_child('dnsmasq.conf'),
        function(w) {
            w('# No DNS functionality');
            w('port=0');
            w('');
            w('dhcp-leasefile=/run/gnome-hwtest/dnsmasq.leases');
            w('dhcp-optsfile=/run/gnome-hwtest/dnsmasq.options');
            w('dhcp-hostsfile=/run/gnome-hwtest/dnsmasq.hosts');
            w('dhcp-match=set:ipxe,175');
            w('dhcp-boot=tag:!ipxe,undionly.kpxe');
            w('dhcp-range=%s,static', config.private_ip);
            w('# 176 == ipxe.no-pxedhcp - should speed up DHCP by not waiting for Proxy DHCP (not sure this works)');
            w('dhcp-option=encap:175,176,1b');
            w('');
            w('enable-tftp=%s', Utils.findIface(config.private_hwaddr));
            w('tftp-root=/usr/share/ipxe');
        });

        Utils.outputToFile(configdir.get_child('dnsmasq.hosts'),
        function(w) {
            for (let machine of Utils.values(config.machines)) {
                w('%s,set:%s,%s', machine.hwaddr, machine.name, machine.ip);
            }
        });

        Utils.outputToFile(configdir.get_child('dnsmasq.options'),
        function(w) {
            for (let machine of Utils.values(config.machines))
                w('tag:%s,tag:ipxe,option:root-path,iscsi:%s:::%d:%s',
                  machine.name, config.private_ip, 1, machine.getBootIqn());
        });

        let pidfile = Gio.File.new_for_path('/var/run/dnsmasq.pid');
        let pid;
        try {
            let [, contents, ] = pidfile.load_contents(null);
            pid = parseInt(String(contents).trim());
        } catch(e) {
        }

        if (pid)
            Utils.runSync(['kill', '-HUP', String(pid)],
                          { stderrSilence: true,
                            checkExit: false });

        return true;
    }
};
