const Format = imports.format;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Lang = imports.lang;

const ConfigFile = imports.configfile;
const LogListener = imports.loglistener;
const Utils = imports.utils;
const Partition = imports.partition;
const Power = imports.power;

const STATE_INACTIVE = 0;
const STATE_UPDATING = 1;
const STATE_TESTING = 2;

const Machine = new Lang.Class({
    Name: 'Machine',

    _init: function(config, name, configfile, group) {
        this.config = config;
        this.name = name;

        this.index = configfile.getInt(group, 'index');
        this.hwaddr = configfile.getString(group, 'hwaddr');
        this.ip = Format.vprintf("10.0.3.%d", [(100 + this.index)]);
        this.state = STATE_INACTIVE;
        this.power_tty = configfile.getString(group, 'power_tty');
        this.power_index = configfile.getInt(group, 'power_index');
        this.partitions = {};

        this._loadBootPartitionName();
    },

    addPartition: function(partition) {
        this.partitions[partition.name] = partition;
    },

    _bootPartitionNameFile: function() {
        return Gio.File.new_for_path('/run/gnome-hwtest/' + this.name + '.boot');
    },

    _loadBootPartitionName: function() {
        let f = this._bootPartitionNameFile();
        if (f.query_exists(null)) {
            let [, contents, ] = f.load_contents(null);
            this.bootPartitionName = String(contents).trim();
        } else {
            this.bootPartitionName = 'minimal';
        }
    },

    setBootPartitionName: function(partitionName) {
        if (partitionName == this.bootPartitionName)
            return;

        this.bootPartitionName = partitionName;
        Utils.outputToFile(this._bootPartitionNameFile(),
                           function(w) {
                               w('%s', partitionName);
                           });

        let dhcp = imports.dhcp;
        dhcp.commands['update-config'](this.config);
    },

    getBootIqn: function() {
        return Partition.bootIqn(this.bootPartitionName);
    },

    writeParams: function(partition, testset) {
        Utils.outputToFile(Gio.File.new_for_path('/run/gnome-hwtest/params/' + this.name + '.params'),
        function(w) {
            w('hwtest_controller_ip=%s', this.config.private_ip);
            w('hwtest_log_port=%s', 35000 + this.index);
            w('hwtest_branch=%s', this.name);
            w('hwtest_partition=%s', partition.name);
            if (testset != null)
                w('hwtest_testset=%s', testset);
        }.bind(this));
    },

    powerOn: function() {
        Power.getController(this.config, this).on(this.power_index);
    },

    powerOff: function() {
        Power.getController(this.config, this).off(this.power_index);
    },

    ensureLogListener: function() {
        if (this.logListener == null)
            this.logListener = new LogListener.LogListener(this.config.private_ip, 35000 + this.index);
    }
});
