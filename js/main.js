let Gio = imports.gi.Gio;

let Config = imports.config;
let ConfigFile = imports.configfile;
let Utils = imports.utils;

let modules = {
    'controller' : 1,
    'dhcp' : 1,
    'http' : 1,
    'iscsi' : 1,
    'network' : 1,
    'power' : 1
};

function main() {
    let config;
    try {
        config = new Config.Config(Gio.file_new_for_path('/srv/gnome-hwtest/controller.conf'));
    } catch (e if e instanceof ConfigFile.ConfigFileError) {
        Utils.printerr("%s\n", e.message);
        return false;
    }

    let module = ARGV[0];
    if (module in modules) {
        let imported = imports[module];

        if (imported.main) {
            return imported.main(config, ARGV.slice(1));
        } else {
            let command = ARGV[1];
            if (command in imported.commands) {
                return imported.commands[command](config, ARGV.slice(2));
            } else {
                Utils.printerr("Usage: gnome-hwtest %s [%s]\n",
                               module,
                               Object.keys(imported.commands).sort().join('|'));
                return false;
            }
    }} else {
        Utils.printerr("Usage: gnome-hwtest [%s]\n",
                          Object.keys(modules).sort().join('|'));
        return false;
    }
}

main() ? 0 : 1;

