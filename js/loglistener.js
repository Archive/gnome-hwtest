const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Signals = imports.signals;

const SHUTDOWN_MESSAGE_ID = '545cc8baba1d4493916a334258c83be0';
const METRIC_MESSAGE_ID = '5eb90e0f00a247d68d95f630814de243';
const FINISHED_MESSAGE_ID = '26a5224162874504864dfa03bf255c8d';


const LogListener = new Lang.Class({
    Name: 'LogListener',

    _init: function(ipaddr, port) {
        this.port = port;
        this.ipaddr = ipaddr;

        let address = Gio.InetAddress.new_from_string(ipaddr);
        let socketAddress = Gio.InetSocketAddress.new(address, port);
        let service = new Gio.SocketService();
        service.add_address(socketAddress,
                            Gio.SocketType.STREAM,
                            Gio.SocketProtocol.TCP,
                            null);
        service.connect('incoming', this._incomingConnection.bind(this));
    },

    _incomingConnection: function(service, connection) {
        let istream = connection.get_input_stream();
        let dataistream = Gio.DataInputStream.new(istream);

        let gotline =
             function(stream, result) {
                 let [line, length] = stream.read_line_finish_utf8(result);
                 if (this._gotLine(line))
                     dataistream.read_line_async(GLib.PRIORITY_DEFAULT, null, gotline);
                 else
                     connection.close(null);
             }.bind(this);

        dataistream.read_line_async(GLib.PRIORITY_DEFAULT, null, gotline);
    },

    _gotLine: function(line) {
        if (!line)
            return false;

        let record = JSON.parse(line);
        this.emit('record', record);
        if (record['MESSAGE_ID'] == SHUTDOWN_MESSAGE_ID)
            return false;
        else
            return true;
    }
});
Signals.addSignalMethods(LogListener.prototype);
