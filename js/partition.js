const Lang = imports.lang;

function bootIqn(name) {
    return 'iqn.2014-04.org.gnome.hwtest:' + name;
}

function bootImage(name) {
    return '/srv/gnome-hwtest/images/boot-' + name + '.image';
}

function bootLabel(name) {
    if (name == 'minimal')
        return 'gnostree-boot';
    else
        return 'hwt-' + name + '-b';
}

const Partition = new Lang.Class({
    Name: 'Partition',

    _init: function(config, machine, name, configfile, group) {
        this.config = config;
        this.machine = machine;
        this.name = name;

        this.tree = configfile.getString(group, 'tree');
        this.testsets = configfile.getString(group, 'testsets', 'x11 wayland').split('/ /');
    }
});

