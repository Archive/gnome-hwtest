const Gio = imports.gi.Gio;
const Lang = imports.lang;
const Mainloop = imports.mainloop;

const TICK_TIME = 60

const AsyncCancel = {
    toString: function() { return "AsyncCancel"; }
};

const Job = new Lang.Class({
    Name: 'Job',

    timeout: -1,
    persists: false,

    _init: function(runner) {
        this.runner = runner;
        this.logger = runner.logger;
        this.running = false;
        this.count = 0;

        this._cancellable = null;
        this._iterator = null;
        this._timeoutId = 0;
    },

    start: function() {
        this.running = true;
        this.run();
        if (this._iterator == None)
            this.stop();
    },

    run: function() {
        if (this.timeout >= 0)
            this._timeoutId = Mainloop.timeout_add(this.timeout * 1000,
                                                   this._asyncTimeout.bind(this));

        this._iterator = this.runAsync(new Gio.Cancellable(),
                                       this._asyncNext.bind(this));

        try {
            this._cancellable = this._iterator.next();
        } catch (e) {
            this._asyncException(e);
        }
    },

    cancel: function() {
        if (this._cancellable)
            this._cancellable.cancel();
        else if (this._iterator) {
            try {
                this._iterator.throw(AsyncCancel);
            } catch (e) {
                this._asyncException(e);
            }
        }
    },

    _asyncNext: function() {
        try {
            this._cancellable = this._iterator.send(arguments);
        } catch (e) {
            this._asyncException(e);
        }
    },

    _asyncException: function(e) {
        if (e instanceof StopIteration) {
            this.stop();
            this.logger.info("job %s: finished", this);
        } else if (e == AsyncCancel ||
                   e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.CANCELLED)) {
            this.stop("Timed out");
            this.logger.error("job %s: timed out", this);
        } else {
            this.stop(String(e), e);
            throw e;
        }
    },

    _asyncTimeout: function(e) {
        this.logger.error("job %s: timed out after %d seconds", this, this.timeout);
        this.cancel();

        return false;
    },

    stop: function(message, exception) {
        this.running = false;
        this._cancellable = null;
        this._iterator = null;

        if (this._timeoutId != 0) {
            Mainloop.source_remove(this._timeoutId);
            this._timeoutId = 0;
        }

        if (!this.persists)
            this.runner.removeJob(this);
    }
});

const JobRunner = new Lang.Class({
    Name: 'JobRunner',

    _init: function(logger) {
        this.logger = logger;
        this.jobs = [];

        Mainloop.timeout_add(TICK_TIME * 1000,
                             function() {
                                 this.checkJobs();
                                 return true;
                             }.bind(this));

        this.checkQueued = false;
        this.queueCheck();
    },

    queueCheck: function() {
        if (!this.checkQueued) {
            this.checkQueued = true;
            Mainloop.timeout_add(0,
                                 function() {
                                     this.checkQueued = false;
                                     this.checkJobs();
                                     return false;
                                 }.bind(this));
        }
    },

    addJob: function(job) {
        this.jobs.push(job);
        this.queueCheck();
        this.logger.info("job %s: added", job);
    },

    removeJob: function(job) {
        let index = this.jobs.indexOf(job);
        if (index >= 0)
            this.jobs.splice(index, 1);
        this.queueCheck();
        this.logger.info("job %s: removed", job);
    },

    checkJobs: function() {
        for (let job of this.jobs) {
            if (!job.running && job.canRun()) {
                this.logger.info("job %s: starting", job);
                job.running = true;
                job.run();
            }
        }
    }
});

const Format = imports.format;
const Utils = imports.utils;

const TestLogger = new Lang.Class({
    Name: 'TestLogger',
    Extends: Utils.Logger,

    _init: function() {
    },

    _log: function(priority, msg, args) {
        printerr('<' + priority + '> ' + Format.vprintf(msg, args));
    }
});

/*
const TestJob = new Lang.Class({
    Name: 'TestJob',
    Extends: Job,

    timeout: 2,

    toString: function() {
        return Format.vprintf("%s(%d)", [this.__name__, this.sleepTime]);
    },

    _init: function(runner, sleepTime) {
        this.parent(runner);
        this.sleepTime = sleepTime;
    },

    canRun: function() {
        return true;
    },

    runAsync: function(cancellable, next) {
        let subprocess = Utils.subprocessCreate(['sleep', String(this.sleepTime)]);
        subprocess.wait_async(cancellable, next);
        let [,result] = yield cancellable;
        subprocess.wait_finish(result);
   }
});

const TestJob2 = new Lang.Class({
    Name: 'TestJob2',
    Extends: Job,

    timeout: 2,

    toString: function() {
        return Format.vprintf("%s(%d)", [this.__name__, this.sleepTime]);
    },

    _init: function(runner, sleepTime) {
        this.parent(runner);
        this.sleepTime = sleepTime;
    },

    canRun: function() {
        return true;
    },

    runAsync: function(cancellable, next) {
        let subprocess = Utils.subprocessCreate(['sleep', String(this.sleepTime)]);
        let id = Mainloop.timeout_add(1000 * this.sleepTime, next);
        try {
            yield;
        } finally {
            Mainloop.source_remove(id);
        }
   }
});

let runner = new JobRunner(new TestLogger());
runner.addJob(new TestJob(runner, 1));
runner.addJob(new TestJob2(runner, 1));
runner.addJob(new TestJob(runner, 5));
runner.addJob(new TestJob2(runner, 5));

Mainloop.run();
*/
