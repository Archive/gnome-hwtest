let Gio = imports.gi.Gio;
let GLib = imports.gi.GLib;
let Format = imports.format;
let Lang = imports.lang;
let Params = imports.params;

let stdin = Gio.UnixInputStream.new(0, false);
let stdout = Gio.UnixOutputStream.new(1, false);
let stderr = Gio.UnixOutputStream.new(2, false);

function RunProcessError(message) {
    this.message = message;
    this.stack = (new Error()).stack;
}

RunProcessError.prototype = Object.create(Error.prototype, {});

function printerr(msg, ...args) {
    let out = Format.vprintf(msg, args);
    stderr.write_bytes(new GLib.Bytes(out), null);
}

function quoteArg(arg) {
    if (/["\s]/.test(arg))
        return '"' + arg.replace(/[\\"]/, "\\$&") + '"';
    else
        return arg;
}

function quoteArgv(argv) {
    return argv.map(quoteArg).join(" ");
}

function subprocessCreate(argv, flags) {
    if (flags === undefined)
        flags = 0;
    let subprocess = Gio.Subprocess.new(argv, flags);
    subprocess.args = argv;
    return subprocess;
}

function subprocessCheck(subprocess) {
    if (subprocess.args == null)
        throw new Error("Subprocess not created with Utils.subprocessCreate()");

    let status = subprocess.get_exit_status();
    if (status != 0)
        throw new RunProcessError(Format.vprintf("%s: failed with exit status %d",
                                                 [quoteArgv(subprocess.args), status]));
 }

function runSync(argv, params) {
    params = Params.parse(params, {
        checkExit: true,
        stderrSilence: false
    });

    let flags = 0;
    if (params.stderrSilence)
        flags |= Gio.SubprocessFlags.STDERR_SILENCE;

    let subprocess = subprocessCreate(argv, flags);
    subprocess.wait(null);

    if (params.checkExit)
        subprocessCheck(subprocess);
}

function iterateLines(instream) {
    let datastream = Gio.DataInputStream.new(instream);
    while (true) {
        let [line, length] = datastream.read_line_utf8(null);
        if (line === null)
            return;
        yield line;
    }
}

function runIterateLines(argv) {
    let subprocess = subprocessCreate(argv, Gio.SubprocessFlags.STDOUT_PIPE);
    for (let line of iterateLines(subprocess.get_stdout_pipe()))
        yield line;

    subprocess.get_stdout_pipe().close(null);

    subprocess.wait(null);
    subprocessCheck(subprocess);
}

function outputToFile(file, callback) {
    let outstream = file.replace(null, false, 0, null);
    let datastream = Gio.DataOutputStream.new(outstream);

    callback(function(fmt, ...args) {
        datastream.put_string(Format.vprintf(fmt, args), null);
        datastream.put_string('\n', null);
    });

    outstream.close(null);
}

function findRootDev() {
    let mounts = Gio.file_new_for_path('/proc/mounts');
    let instream = mounts.read(null);
    try {
        for (let line of iterateLines(instream)) {
            let words = line.trim().split(/\s+/);
            if (words[1] == '/' && words[0] != 'rootfs')
                return words[0];
        }
        return null;
    } finally {
        instream.close(null);
    }
}

function findIface(hwaddr) {
    hwaddr = hwaddr.toLowerCase();

    let netdir = Gio.file_new_for_path('/sys/class/net');
    let enumerator = netdir.enumerate_children('standard::name', Gio.FileQueryInfoFlags.NONE, null);

    try {
        while (true) {
            let info = enumerator.next_file(null);
            if (info == null)
                break;
            let child = enumerator.get_child(info);
            let [, contents,, ] = child.get_child('address').load_contents(null);
            if (String(contents).trim() == hwaddr)
                return child.get_basename();
        }
    } finally {
        enumerator.close(null);
    }

    return null;
}

function ifaceAddr(iface) {
    let addr = null;

    for (let line of runIterateLines(['ifconfig', iface])) {
        let m = /inet[^0-9]+([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/.exec(line);
        if (m)
            addr = m[1];
    }

    return addr;
}

function systemdUnitStatus(unit) {
    let state = 'inactive';

    for (let line in runIterateLines(['systemctl', 'show', '--property=ActiveState', unit])) {
        let m = /^ActiveState=(.*)/.exec(line);
        if (m)
            state = m[1];
    }

    return state;
}

function values(o) {
    for (let k in o) {
        if (Object.prototype.hasOwnProperty.call(o, k)) {
            yield o[k];
        }
    }
}

function mkdir_p(file) {
    try {
        file.make_directory_with_parents(null);
    } catch (e if e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.EXISTS)) {
    }
}

const Logger = new Lang.Class({
    Name: 'Logger',

    _init: function(identifier) {
        this.isTTY = false;

        let stdoutFile;
        try {
            stdoutFile = GLib.file_read_link('/proc/self/fd/1');
        } catch (e) {
        }

        if (stdoutFile &&
            stdoutFile.startsWith('/dev/tty') || stdoutFile.startsWith('/dev/pts'))
            this.isTTY = true;

        if (this.isTTY) {
            this.stream = Gio.DataOutputStream.new(stderr);
        } else {
            this.subprocess = subprocessCreate(['systemd-cat', '-t', identifier],
                                               Gio.SubprocessFlags.STDIN_PIPE);
            this.stream = Gio.DataOutputStream.new(this.subprocess.get_stdin_pipe());

        }
    },

    _log: function(priority, fmt, args) {
        this.stream.put_string("<" + priority + ">", null);
        this.stream.put_string(Format.vprintf(fmt, args), null);
        this.stream.put_string("\n", null);
        this.stream.flush(null);
    },

    error: function(fmt, ...args) {
        this._log(3, fmt, args);
    },

    warning: function(fmt, ...args) {
        this._log(4, fmt, args);
    },

    notice: function(fmt, ...args) {
        this._log(5, fmt, args);
    },

    info: function(fmt, ...args) {
        this._log(6, fmt, args);
    },

    debug: function(fmt, ...args) {
        this._log(7, fmt, args);
    }
});
