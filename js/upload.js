let Format = imports.format;
let GLib = imports.gi.GLib;
let Gio = imports.gi.Gio;
let Lang = imports.lang;
let Soup = imports.gi.Soup;
let Utils = imports.utils;

// Encode everything but ~._-
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent
function encodeStrictly(str) {
  return encodeURIComponent(str).replace(/[!'()]/g, escape).replace(/\*/g, "%2A");
}

const Uploader = new Lang.Class({
    Name: 'Uploader',

    _init: function(config) {
        this.config = config;
        this.session = Soup.Session.new();
    },

    _makeSignature: function(url, data, privkeyfile) {
        let signature_material = 'POST&' + encodeStrictly(url) + '&&' + data;

        let sha256 = Utils.subprocessCreate(['openssl', 'sha256', '-sign', privkeyfile],
                                            Gio.SubprocessFlags.STDIN_PIPE | Gio.SubprocessFlags.STDOUT_PIPE);
        sha256.get_stdin_pipe().splice(Gio.MemoryInputStream.new_from_bytes(new GLib.Bytes(signature_material)),
                                       Gio.OutputStreamSpliceFlags.CLOSE_SOURCE | Gio.OutputStreamSpliceFlags.CLOSE_TARGET,
                                       null);

        let base64 = Utils.subprocessCreate(['openssl', 'base64', '-A'],
                                            Gio.SubprocessFlags.STDIN_PIPE | Gio.SubprocessFlags.STDOUT_PIPE);
        base64.get_stdin_pipe().splice(sha256.get_stdout_pipe(),
                                       Gio.OutputStreamSpliceFlags.CLOSE_SOURCE | Gio.OutputStreamSpliceFlags.CLOSE_TARGET,
                                       null);

        let output = base64.get_stdout_pipe().read_bytes(1024, null);
        let signature_base64 = String(output.toArray());
        base64.get_stdout_pipe().close(null);

        sha256.wait(null);
        Utils.subprocessCheck(sha256);
        base64.wait(null);
        Utils.subprocessCheck(base64);

        return "RSA-SHA256 "+ signature_base64;
    },

    _formatTime: function(time) {
        return Format.vprintf("%04d-%02d-%02d %02d:%02d:%02d",
                              [time.getUTCFullYear(),
                               time.getUTCMonth() + 1,
                               time.getUTCDate(),
                               time.getUTCHours(),
                               time.getUTCMinutes(),
                               time.getUTCSeconds()]);
    },

    _uploadAsync: function(machine, jsonData, cancellable, callback) {
        let url = this.config.perf_server_api + '/upload?machine=' + machine.name;
        let data  = JSON.stringify(jsonData);
        let privkeyfile = '/srv/gnome-hwtest/' + machine.name + '.secret';

        let signature = this._makeSignature(url, data, privkeyfile);

        let msg = Soup.Message.new ("POST", url);
        msg.set_request ("application/json", Soup.MemoryUse.COPY, data);
        msg.request_headers.append("X-GNOME-Perf-Signature", signature);
        this.session.send_async(msg, cancellable, function(session, result) {
            callback(this, {
                message: msg,
                soupResult: result
            });
        }.bind(this));
    },

    uploadSuccessAsync: function(partition, tree, testset, revision, pullTime, metrics, cancellable, callback) {
        let report = {
            "machine": partition.machine.name,
            "partition": partition.name,
            "tree": tree,
            "testset": testset,
            "revision": revision,
            "pullTime": this._formatTime(pullTime),
            "metrics": metrics
        }

        this._uploadAsync(partition.machine, report, cancellable, callback);
    },

    uploadErrorAsync: function(partition, tree, testset, revision, pullTime, errorMessage, log, cancellable, callback) {
        let report = {
            "machine": partition.machine.name,
            "partition": partition.name,
            "tree": tree,
            "testset": testset,
            "revision": revision,
            "pullTime": this._formatTime(pullTime),
            "error": errorMessage
        }

        if (log != null) {
            report['log'] = log;
        }

        this._uploadAsync(partition.machine, report, cancellable, callback);
    },

    uploadFinish: function(result) {
        let stream = this.session.send_finish(result.soupResult);
        if (result.message.status_code != 200) {
            let output = stream.read_bytes(64*1024, null);
            Utils.stderr.write_bytes(output, null);
            stream.close(null);
            throw new Error(Format.vprintf("Failed to post to server : %d %s\n",
                                           [result.message.status_code, result.message.reason_phrase]));
        }

        stream.close(null);
    },
});

function getUploader(config) {
    if (config._Uploader == null)
        config._Uploader = new Uploader(config);

    return config._Uploader;
}
