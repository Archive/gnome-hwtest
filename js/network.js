let Gio = imports.gi.Gio
let GLib = imports.gi.GLib;

let Utils = imports.utils;

const PRIVATE_IP = '10.0.3.2';
const PRIVATE_NETMASK = '255.255.255.0';

let commands = {
    'start' : function(config) {
        let private_iface = Utils.findIface(config.private_hwaddr);
        if (private_iface == null) {
            Utils.printerr("Cannot find private hwaddr %s\n", config.private_hwaddr);
            return false;
        }

        let public_iface = Utils.findIface(config.public_hwaddr);
        if (public_iface == null) {
            Utils.printerr("Cannot find public hwaddr %s\n", config.public_hwaddr);
            return false;
        }

        Utils.runSync(['ifconfig', private_iface, PRIVATE_IP, 'netmask', PRIVATE_NETMASK]);
        Utils.runSync(['udhcpc', '-i', public_iface]);

        let carrierFile = Gio.file_new_for_path('/sys/class/net/' + private_iface + '/carrier');
        while (true) {
            let [, contents ,] = carrierFile.load_contents(null);
            if (String(contents).trim() != '0')
                break;
            GLib.usleep(100000);
        }

        let environ = Gio.file_new_for_path('/run/gnome-hwtest/network.environ');
        Utils.mkdir_p(environ.get_parent());
        Utils.outputToFile(environ,
        function(w) {
            w('hwtest_private_ip=%s', PRIVATE_IP);
        });
        return true;
    },
};
