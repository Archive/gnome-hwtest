#define SD_JOURNAL_SUPPRESS_LOCATION

#include <stdarg.h>
#include <systemd/sd-journal.h>
#include <glib.h>

static const char * const priorities[] = {
  "emerg", "alert", "crit", "err", "warning", "notice", "info", "debug", NULL
};

typedef struct {
  size_t offset;
  size_t len;
} SendOffset;

static GArray *send_offsets;
static GString *buffer;

static void
add_field(const char *key_equal_value)
{
  SendOffset offset;
  offset.offset = buffer->len;
  g_string_append (buffer, key_equal_value);
  offset.len = buffer->len - offset.offset;
  g_array_append_val (send_offsets, offset);
}


static void  add_fieldf(const char *format,
                        ...) G_GNUC_PRINTF(1, 0);

static void
add_fieldf(const char *format,
           ...)
{
  SendOffset offset;
  va_list args;

  offset.offset = buffer->len;

  va_start (args, format);
  g_string_append_vprintf (buffer, format, args);
  va_end (args);

  offset.len = buffer->len - offset.offset;
  g_array_append_val (send_offsets, offset);
}

int main(int argc, char **argv)
{
  static const char *metric = NULL;
  static const char *metric_description = NULL;
  static const char *identifier = NULL;
  static const char *priority_str = NULL;
  static gboolean finished = FALSE;
  static gboolean shutdown = FALSE;
  int priority = LOG_INFO;
  static GOptionEntry entries[] =
    {
      { "identifier", 't', 0, G_OPTION_ARG_STRING, &identifier, "Source of the log message", "IDENTIFIER" },
      { "priority", 'p', 0, G_OPTION_ARG_STRING, &priority_str, "Priority (emerg|alert|crit|err|warning|notice|info|debug|0..7) - default info", "PRIORITY" },
      { "metric", 0, 0, G_OPTION_ARG_STRING, &metric, "Log a metric", "NAME=VALUE[UNITS]" },
      { "metric-description", 0, 0, G_OPTION_ARG_STRING, &metric_description, "Description of metric", "DESC" },
      { "finished", 0, 0, G_OPTION_ARG_NONE, &finished, "Indicate that a task has finished" },
      { "shutdown", 0, 0, G_OPTION_ARG_NONE, &shutdown, "Indicate that that the system is shutting down" },
    };

  GOptionContext *context;
  GError *error = NULL;
  GArray *send_iov;
  int i;

  context = g_option_context_new ("FIELD1=VALUE1 FIELD2=VALUE2...");
  g_option_context_add_main_entries (context, entries, NULL);
  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("option parsing failed: %s\n", error->message);
      return 1;
    }

  send_offsets = g_array_new (FALSE, FALSE, sizeof (SendOffset));
  buffer = g_string_new (NULL);

  if (identifier != NULL)
    add_fieldf("SYSLOG_IDENTIFIER=%s", identifier);

  if (priority_str != NULL)
    {
      for (i = 0; priorities[i]; i++)
        {
          if (strcmp (priorities[i], priority_str) == 0)
            {
              priority = i;
              break;
            }
        }

      if (priorities[i] == NULL)
        {
          if (priority_str[0] >= '0' && priority_str[0] <= '7' && priority_str[1] == 0)
            {
              priority = priority_str[0] - '0';
            }
          else
            {
              g_printerr ("Unknown priority '%s'\n", priority_str);
              return 1;
            }
        }
    }

  add_fieldf("PRIORITY=%d", priority);

  if ((metric ? 1 : 0) +
      (finished ? 1 : 0) +
      (shutdown ? 1 : 0) > 1)
    {
      g_printerr ("Only one of --metric, --finished, and --shutdown can be specified\n");
      return 1;
    }

  if (metric)
    {
      const char *equals;
      char *metric_name;
      char *metric_value;
      const char *metric_units = NULL;
      char *endptr;
      double value G_GNUC_UNUSED;

      equals = strchr (metric, '=');
      if (equals == NULL)
        {
          g_printerr ("--metric argument should be of the form NAME=VALUE[UNITS]\n");
          return 1;
        }

      metric_name = g_strndup (metric, equals - metric);

      value = g_ascii_strtod (equals + 1, &endptr);
      if (endptr == equals + 1)
        {
          g_printerr ("--metric argument should be of the form NAME=VALUE[UNITS]\n");
          return 1;
        }

      metric_value = g_strndup (equals + 1, endptr - (equals + 1));
      if (*endptr)
        metric_units = endptr;

      add_fieldf ("MESSAGE_ID=5eb90e0f00a247d68d95f630814de243");
      add_fieldf ("METRIC_NAME=%s", metric_name);
      add_fieldf ("METRIC_VALUE=%s", metric_value);
      if (metric_units)
        add_fieldf ("METRIC_UNITS=%s", metric_units);
    }

  if (metric_description)
    {
      if (!metric)
        {
          g_printerr("--metric-description requires --metric\n");
          return 1;
        }

      add_fieldf("METRIC_DESCRIPTION=%s", metric);
    }

  if (metric_description)
    add_fieldf("MESSAGE=%s (%s)", metric, metric_description);
  else if (metric)
    add_fieldf("MESSAGE=%s", metric);

  if (finished)
    {
      add_fieldf ("MESSAGE_ID=26a5224162874504864dfa03bf255c8d");
      add_fieldf ("MESSAGE=Finished");
    }

  if (shutdown)
    {
      add_fieldf ("MESSAGE_ID=545cc8baba1d4493916a334258c83be0");
      add_fieldf ("MESSAGE=System shutdown");
    }

  for (i = 1; i < argc; i++)
    {
      const char *equals = strchr (argv[i], '=');
      const char *p;
      if (equals == NULL)
        {
          g_printerr ("argument '%s' should be of the form FIELD=VALUE\n", argv[i]);
          return 1;
        }
      for (p = argv[i]; p != equals; p++)
        {
          if (!((*p >= 'A' && *p <= 'Z') ||
                (*p >= '0' && *p <= '9') ||
                (p != argv[i] && *p == '_')))
            {
              g_printerr ("Field name should consist of upper-case letters, digits, and underscores, and not start with an underscore\n");
              return 1;
            }
        }

      add_field (argv[i]);
    }

  send_iov = g_array_new (FALSE, FALSE, sizeof (struct iovec));
  for (i = 0; i < send_offsets->len; i++)
    {
      SendOffset *offset = &g_array_index (send_offsets, SendOffset, i);
      struct iovec iov;
      iov.iov_base = buffer->str + offset->offset;
      iov.iov_len = offset->len;
      g_array_append_val (send_iov, iov);
    }

  sd_journal_sendv ((struct iovec *)send_iov->data, send_iov->len);

  return 0;
}
