/* $XTermId: resize.c,v 1.130 2014/04/25 23:39:42 tom Exp $ */

/*
 * This is a copy of resize.c from the xterm distribution, stripped down
 * in the following ways:
 *
 *  - This version only supports POSIX termios
 *  - Other portability checks for cygwin, VMS, QNX, etc, have been removed.
 *  - The 'sunsize' alias and the -s option have been removed along
 *    with the support for Sun escape sequences - only VT100 is supported.
 *
 * No code was added in the process, so the following copyrights apply.
 *
 * Copyright 2003-2013,2014 by Thomas E. Dickey
 *
 *                         All Rights Reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE ABOVE LISTED COPYRIGHT HOLDER(S) BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders shall not be used in advertising or otherwise to promote the
 * sale, use or other dealings in this Software without prior written
 * authorization.
 *
 *
 * Copyright 1987 by Digital Equipment Corporation, Maynard, Massachusetts.
 *
 *                         All Rights Reserved
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and that
 * both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of Digital Equipment
 * Corporation not be used in advertising or publicity pertaining to
 * distribution of the software without specific, written prior permission.
 *
 *
 * DIGITAL DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * DIGITAL BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

/* resize.c */
#include <errno.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include <unistd.h>

#include <pwd.h>

#include <sys/ioctl.h>
#include <termios.h>

#define ENVP_ARG /**/

typedef unsigned char Char;		/* to support 8 bit chars */

typedef int Boolean;
typedef char *String;
#define False (0)
#define True (!False)

#define IsEmpty(s) ((s) == 0 || *(s) == '\0')
#define IsSpace(c) ((c) == ' ' || (c) == '\t' || (c) == '\r' || (c) == '\n')
#define CharOf(n) ((Char)(n))

#if defined(__GNUC__) && defined(_FORTIFY_SOURCE)
#define USE_IGNORE_RC
#define IGNORE_RC(func) ignore_unused = (int) func
#else
#define IGNORE_RC(func) (void) func
#endif /* gcc workarounds */

#ifndef GCC_UNUSED
#define GCC_UNUSED		/* nothing */
#endif

#define DFT_TERMTYPE "xterm"

#define UIntClr(dst,bits) dst = dst & (unsigned) ~(bits)
#define TypeMallocN(type,n)	(type *)malloc(sizeof(type) * (size_t) (n))
#define TypeCallocN(type,n)	(type *)calloc((size_t) (n), sizeof(type))
#define TypeCalloc(type)	TypeCallocN(type, 1)
#define CastMallocN(type,n)	(type *)malloc(sizeof(type) + (size_t) (n))

#define xtermVersion() "GNOME Hardware Testing (xterm-304)"

#ifndef TRACE2
#define TRACE2(p) /*nothing*/
#endif
#define OkPasswd(p) ((p)->pw_name != 0 && (p)->pw_name[0] != 0)

static Boolean x_getpwnam(const char * /* name */, struct passwd * /* result */);
static Boolean x_getpwuid(uid_t /* uid */, struct passwd * /* result */);
static String x_nonempty(String /* s */);
static String x_skip_blanks(String /* s */);
static char *x_basename(char * /* name */);
static char *x_getenv(const char * /* name */);
static char *x_getlogin(uid_t /* uid */, struct passwd * /* in_out */);

#include <signal.h>
#include <pwd.h>

#ifdef USE_IGNORE_RC
int ignore_unused;
#endif

#define ESCAPE(string) "\033" string

#define	TIMEOUT		10

#define	SHELL_UNKNOWN	0
#define	SHELL_C		1
#define	SHELL_BOURNE	2
/* *INDENT-OFF* */
static struct {
    const char *name;
    int type;
} shell_list[] = {
    { "csh",	SHELL_C },	/* vanilla cshell */
    { "jcsh",   SHELL_C },
    { "tcsh",   SHELL_C },
    { "sh",	SHELL_BOURNE }, /* vanilla Bourne shell */
    { "ash",    SHELL_BOURNE },
    { "bash",	SHELL_BOURNE }, /* GNU Bourne again shell */
    { "dash",	SHELL_BOURNE },
    { "jsh",    SHELL_BOURNE },
    { "ksh",	SHELL_BOURNE }, /* Korn shell (from AT&T toolchest) */
    { "ksh-i",	SHELL_BOURNE }, /* another name for Korn shell */
    { "ksh93",	SHELL_BOURNE }, /* Korn shell */
    { "mksh",   SHELL_BOURNE },
    { "pdksh",  SHELL_BOURNE },
    { "zsh",    SHELL_BOURNE },
    { NULL,	SHELL_BOURNE }	/* default (same as xterm's) */
};
/* *INDENT-ON* */

static char *myname;
static int shell_type = SHELL_UNKNOWN;
static const char *getsize =
    ESCAPE("7") ESCAPE("[r") ESCAPE("[999;999H") ESCAPE("[6n");
static const char *restore =
    ESCAPE("8");
static const char *setname = "";

static struct termios tioorig;

static const char *size =
    ESCAPE("[%d;%dR");

static int tty;
static FILE *ttyfp;

static void
failed(const char *s)
{
    int save = errno;
    IGNORE_RC(write(2, myname, strlen(myname)));
    IGNORE_RC(write(2, ": ", (size_t) 2));
    errno = save;
    perror(s);
    exit(EXIT_FAILURE);
}

/* ARGSUSED */
static void
onintr(int sig GCC_UNUSED)
{
    (void) tcsetattr(tty, TCSADRAIN, &tioorig);
    exit(EXIT_FAILURE);
}

static void
resize_timeout(int sig)
{
    fprintf(stderr, "\n%s: Time out occurred\r\n", myname);
    onintr(sig);
}

static void
Usage(void)
{
    fprintf(stderr,
	    "Usage: %s [-v] [-u] [-c]\n", myname);
    exit(EXIT_FAILURE);
}

static void
readstring(FILE *fp, char *buf, const char *str)
{
    int last, c;
    /* What is the advantage of setitimer() over alarm()? */
    struct itimerval it;

    signal(SIGALRM, resize_timeout);
    memset((char *) &it, 0, sizeof(struct itimerval));
    it.it_value.tv_sec = TIMEOUT;
    setitimer(ITIMER_REAL, &it, (struct itimerval *) NULL);
    if ((c = getc(fp)) == 0233) {	/* meta-escape, CSI */
	c = ESCAPE("")[0];
	*buf++ = (char) c;
	*buf++ = '[';
    } else {
	*buf++ = (char) c;
    }
    if (c != *str) {
	fprintf(stderr, "%s: unknown character, exiting.\r\n", myname);
	onintr(0);
    }
    last = str[strlen(str) - 1];
    while ((*buf++ = (char) getc(fp)) != last) {
	;
    }
    memset((char *) &it, 0, sizeof(struct itimerval));
    setitimer(ITIMER_REAL, &it, (struct itimerval *) NULL);
    *buf = 0;
}

static char *
x_basename(char *name)
{
    char *cp;

    cp = strrchr(name, '/');
    return (cp ? cp + 1 : name);
}

static char *
x_getenv(const char *name)
{
    char *result;
    result = strdup(x_nonempty(getenv(name)));
    TRACE2(("getenv(%s) %s\n", name, result));
    return result;
}

/*
 * Check if the given string is nonnull/nonempty.  If so, return a pointer
 * to the beginning of its content, otherwise return null.
 */
static String
x_nonempty(String s)
{
    if (s != 0) {
	if (*s == '\0') {
	    s = 0;
	} else {
	    s = x_skip_blanks(s);
	    if (*s == '\0')
		s = 0;
	}
    }
    return s;
}

static String
x_skip_blanks(String s)
{
    while (IsSpace(CharOf(*s)))
	++s;
    return s;
}

static void
alloc_pw(struct passwd *target, struct passwd *source)
{
    *target = *source;
    /* we care only about these strings */
    target->pw_dir = strdup(source->pw_dir);
    target->pw_name = strdup(source->pw_name);
    target->pw_shell = strdup(source->pw_shell);
}

/*
 * Simpler than getpwnam_r, retrieves the passwd result by name and stores the
 * result via the given pointer.  On failure, wipes the data to prevent use.
 */
Boolean
x_getpwnam(const char *name, struct passwd *result)
{
    struct passwd *ptr = getpwnam(name);
    Boolean code;

    if (ptr != 0 && OkPasswd(ptr)) {
	code = True;
	alloc_pw(result, ptr);
    } else {
	code = False;
	memset(result, 0, sizeof(*result));
    }
    return code;
}

/*
 * Simpler than getpwuid_r, retrieves the passwd result by uid and stores the
 * result via the given pointer.  On failure, wipes the data to prevent use.
 */
static Boolean
x_getpwuid(uid_t uid, struct passwd *result)
{
    struct passwd *ptr = getpwuid((uid_t) uid);
    Boolean code;

    if (ptr != 0 && OkPasswd(ptr)) {
	code = True;
	alloc_pw(result, ptr);
    } else {
	code = False;
	memset(result, 0, sizeof(*result));
    }
    TRACE2(("x_getpwuid(%d) %d\n", (int) uid, (int) code));
    return code;
}

static char *
login_alias(char *login_name, uid_t uid, struct passwd *in_out)
{
    /*
     * If the logon-name differs from the value we get by looking in the
     * password file, check if it does correspond to the same uid.  If so,
     * allow that as an alias for the uid.
     */
    if (!IsEmpty(login_name)
	&& strcmp(login_name, in_out->pw_name)) {
	struct passwd pw2;

	if (x_getpwnam(login_name, &pw2)) {
	    uid_t uid2 = pw2.pw_uid;
	    struct passwd pw3;

	    if (x_getpwuid(uid, &pw3)
		&& ((uid_t) pw3.pw_uid == uid2)) {
		/* use the other passwd-data including shell */
		alloc_pw(in_out, &pw2);
	    } else {
		free(login_name);
		login_name = NULL;
	    }
	}
    }
    return login_name;
}

/*
 * Call this with in_out pointing to data filled in by x_getpwnam() or by
 * x_getpwnam().  It finds the user's logon name, if possible.  As a side
 * effect, it updates in_out to fill in possibly more-relevant data, i.e.,
 * in case there is more than one alias for the same uid.
 */
static char *
x_getlogin(uid_t uid, struct passwd *in_out)
{
    char *login_name = NULL;

    login_name = login_alias(x_getenv("LOGNAME"), uid, in_out);
    if (IsEmpty(login_name)) {
	free(login_name);
	login_name = login_alias(x_getenv("USER"), uid, in_out);
    }

    /*
     * Of course getlogin() will fail if we're started from a window-manager,
     * since there's no controlling terminal to fuss with.  For that reason, we
     * tried first to get something useful from the user's $LOGNAME or $USER
     * environment variables.
     */
    if (IsEmpty(login_name)) {
	TRACE2(("...try getlogin\n"));
	free(login_name);
	login_name = login_alias(strdup(getlogin()), uid, in_out);
    }

    if (IsEmpty(login_name)) {
	free(login_name);
	login_name = strdup(in_out->pw_name);
    }

    TRACE2(("x_getloginid ->%s\n", NonNull(login_name)));
    return login_name;
}

/*
   resets termcap string to reflect current screen size
 */
int
main(int argc, char **argv ENVP_ARG)
{
    char *ptr;
    char *shell;
    int i;
    int rc;
    int rows, cols;
    struct termios tio;
    char buf[BUFSIZ];
    struct winsize ts;

    myname = x_basename(argv[0]);
    for (argv++, argc--; argc > 0 && **argv == '-'; argv++, argc--) {
	switch ((*argv)[1]) {
	case 'u':		/* Bourne (Unix) shell */
	    shell_type = SHELL_BOURNE;
	    break;
	case 'c':		/* C shell */
	    shell_type = SHELL_C;
	    break;
	case 'v':
	    printf("%s\n", xtermVersion());
	    exit(EXIT_SUCCESS);
	default:
	    Usage();		/* Never returns */
	}
    }

    if (SHELL_UNKNOWN == shell_type) {
	/* Find out what kind of shell this user is running.
	 * This is the same algorithm that xterm uses.
	 */
	if ((ptr = x_getenv("SHELL")) == NULL) {
	    uid_t uid = getuid();
	    struct passwd pw;

	    if (x_getpwuid(uid, &pw)) {
		(void) x_getlogin(uid, &pw);
	    }
	    if (!OkPasswd(&pw)
		|| *(ptr = pw.pw_shell) == 0) {
		/* this is the same default that xterm uses */
		ptr = strdup("/bin/sh");
	    }
	}

	shell = x_basename(ptr);

	/* now that we know, what kind is it? */
	for (i = 0; shell_list[i].name; i++) {
	    if (!strcmp(shell_list[i].name, shell)) {
		break;
	    }
	}
	shell_type = shell_list[i].type;
    }

    if (argc != 0) {
	Usage();		/* Never returns */
    }

    if ((ttyfp = fopen("/dev/tty", "r+")) == NULL) {
        fprintf(stderr, "%s:  can't open terminal /dev/tty\n",
		myname);
	exit(EXIT_FAILURE);
    }
    tty = fileno(ttyfp);
    if (x_getenv("TERM") == 0) {
	if (SHELL_BOURNE == shell_type) {
	    setname = "TERM=" DFT_TERMTYPE ";\nexport TERM;\n";
	} else {
	    setname = "setenv TERM " DFT_TERMTYPE ";\n";
	}
    }

    rc = tcgetattr(tty, &tioorig);
    tio = tioorig;
    UIntClr(tio.c_iflag, ICRNL);
    UIntClr(tio.c_lflag, (ICANON | ECHO));
    tio.c_cflag |= CS8;
    tio.c_cc[VMIN] = 6;
    tio.c_cc[VTIME] = 1;
    if (rc != 0)
	failed("get tty settings");

    signal(SIGINT, onintr);
    signal(SIGQUIT, onintr);
    signal(SIGTERM, onintr);

    rc = tcsetattr(tty, TCSADRAIN, &tio);
    if (rc != 0)
	failed("set tty settings");

    IGNORE_RC(write(tty, getsize, strlen(getsize)));
    readstring(ttyfp, buf, size);
    if (sscanf(buf, size, &rows, &cols) != 2) {
	fprintf(stderr, "%s: Can't get rows and columns\r\n", myname);
	onintr(0);
    }
    IGNORE_RC(write(tty, restore, strlen(restore)));
    /* finally, set the tty's window size */
    if (ioctl(tty, TIOCGWINSZ, &ts) != -1) {
	/* we don't have any way of directly finding out
	   the current height & width of the window in pixels.  We try
	   our best by computing the font height and width from the "old"
	   window-size values, and multiplying by these ratios... */
	if (ts.ws_col != 0)
	    ts.ws_xpixel = (unsigned short) (cols * (ts.ws_xpixel / ts.ws_col));
	if (ts.ws_row != 0)
	    ts.ws_ypixel = (unsigned short) (rows * (ts.ws_ypixel / ts.ws_row));
	ts.ws_row = (unsigned short) rows;
	ts.ws_col = (unsigned short) cols;
        ioctl(tty, TIOCSWINSZ, (char *) &ts);
    }

    rc = tcsetattr(tty, TCSADRAIN, &tioorig);
    if (rc != 0)
	failed("set tty settings");

    signal(SIGINT, SIG_DFL);
    signal(SIGQUIT, SIG_DFL);
    signal(SIGTERM, SIG_DFL);

    if (SHELL_BOURNE == shell_type) {
	printf("%sCOLUMNS=%d;\nLINES=%d;\nexport COLUMNS LINES;\n",
	       setname, cols, rows);
    } else {			/* not Bourne shell */
	printf("set noglob;\n%ssetenv COLUMNS '%d';\nsetenv LINES '%d';\nunset noglob;\n",
	       setname, cols, rows);
    }
    exit(EXIT_SUCCESS);
}
